const { readdirSync } = require('fs');

const getGalleryNames = () =>
  readdirSync(`${__dirname}/../public`, { withFileTypes: true })
    .filter(file => file.isDirectory())
    .map(directory => directory.name)

const getImageIds = (gallery, limit = 100, page = 1) =>
  readdirSync(`${__dirname}/../public/${gallery}`)
    .filter(image => image.includes('jpg'))
    .slice((page - 1) * limit, page * limit)


const getImageCount = gallery =>
  readdirSync(`${__dirname}/../public/${gallery}`)
    .filter(image => image.includes('jpg'))
    .length;


module.exports = {
  getGalleryNames,
  getImageIds,
  getImageCount
}