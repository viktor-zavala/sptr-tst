const express = require("express");

const { getGalleries, getImagesForGallery, getImageCountForGallery } = require('./handlers/galleries');

const PORT = process.env.PORT || 3001;

const app = express();

app.use('/static', express.static('public'))
app.get('/galleries', getGalleries)
app.get('/:gallery/images', getImagesForGallery)
app.get('/:gallery/image_count', getImageCountForGallery)
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});