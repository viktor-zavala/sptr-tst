const { response } = require('express');
const { getGalleryNames, getImageIds, getImageCount } = require('../services/galleries');

const getGalleries = (request, reply) => {
  const galleries = getGalleryNames();
  reply.json({ galleries });
}

const getImagesForGallery = (request, reply) => {
  const { gallery } = request.params;
  const { limit, page } = request.query;
  const images = getImageIds(gallery, limit, page);
  reply.json({ images });
}

const getImageCountForGallery = (request, reply) => {
  const { gallery } = request.params;
  const imageCount = getImageCount(gallery);
  reply.json({ imageCount })
}

module.exports = {
  getGalleries,
  getImagesForGallery,
  getImageCountForGallery
}