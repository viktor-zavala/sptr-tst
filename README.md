# Spotr test

- [Design reference](https://www.figma.com/file/ghW8ZuAu8u7b9OZrjVwtKO/Spotr.ai-Full-Stack-assignment?node-id=1%3A3258)
- [Assignment](https://spotr-ai.slite.com/p/note/Xk8KzTYcGpnQtzEpbovvtf)

## How to run

On one terminal
1. Go into /client
2. run `yarn`
3. run `yarn start` after installation

On a separate terminal
1. Go into `/server`
2. run `yarn`
3. run `yarn start`

Visit `http://localhost:3000` on a browser