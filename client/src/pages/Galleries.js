import React, { useEffect, useState } from 'react';
import { getGalleries } from '../services/galleries';
import GalleryPreview from './GalleryPreview';

const Galleries = () => {
  const [galleries, setGalleries] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const galleries = await getGalleries();
      setGalleries(galleries);
    }
    fetchData();
  }, []);

  return (
    <div>
      {galleries.map(gallery => (
        <GalleryPreview gallery={gallery} />
      ))}
    </div>
  );
};

export default Galleries;
