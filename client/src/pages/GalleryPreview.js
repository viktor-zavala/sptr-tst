import React, { useState, useEffect } from 'react';
import { getPreviewImages, getGalleryData, getGalleryImageCount } from '../services/galleries';
import GalleryTopBar from '../components/GalleryTopBar';

const GalleryPreview = ({ gallery }) => {
  const [images, setImages] = useState([]);
  const [galleryData, setGalleryData] = useState({});
  const [totalImageCount, setTotalImageCount] = useState(0);
  useEffect(() => {
    const fetchData = async () => {
      const imgNames = await getPreviewImages(gallery);
      const data = await getGalleryData(gallery);
      const totalImages = await getGalleryImageCount(gallery);
      setImages(imgNames);
      setGalleryData(data);
      setTotalImageCount(totalImages);
    }
    fetchData()
  }, [gallery]);

  return (
    <>
      <GalleryTopBar galleryData={galleryData} totalImageCount={totalImageCount} gallery={gallery} preview />
      <div className="gallery-preview__container">
        {images.map(image => (
          <div className="gallery-preview__image-container">
            <img className="gallery-preview__image" src={`/static/${gallery}/${image}`} alt={image}/>
          </div>
        ))}
      </div>
    </>
  )
};

export default GalleryPreview;