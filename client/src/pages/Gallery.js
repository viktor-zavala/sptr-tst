import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getImages, getGalleryData, getGalleryImageCount } from '../services/galleries';
import GalleryTopBar from '../components/GalleryTopBar';

const Gallery = () => {
  const { gallery } = useParams()
  const [images, setImages] = useState([]);
  const [galleryData, setGalleryData] = useState({});
  const [totalImageCount, setTotalImageCount] = useState(0);
  useEffect(() => {
    const fetchData = async () => {
      const imgNames = await getImages(gallery);
      const data = await getGalleryData(gallery);
      const totalImages = await getGalleryImageCount(gallery);
      setImages(imgNames);
      setGalleryData(data);
      setTotalImageCount(totalImages);
    }
    fetchData()
  }, [gallery])
  return (
    <>
      <GalleryTopBar galleryData={galleryData} gallery={gallery} totalImageCount={totalImageCount} />
      <div className="gallery__container">
        {images.map(image => (
          <div className="gallery__image-container">
            <img className="gallery__image" src={`/static/${gallery}/${image}`} alt={image}/>
          </div>
        ))}
      </div>
    </>
  )
}

export default Gallery