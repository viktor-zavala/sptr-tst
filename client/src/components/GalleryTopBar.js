import React from 'react';
import { Link } from 'react-router-dom'

const GalleryTopBar = ({ gallery, galleryData, totalImageCount, preview }) => (
  <div className="gallery-preview__top-bar">
    <div className="gallery-preview__title-container">
      {!preview && (
        <Link className="gallery-top-bar__go-back-link" to="/" >
          <img src={`${process.env.PUBLIC_URL}/left-arrow.svg`} alt={`Back to galleries`}/>
        </Link>
      )}
      <img className="gallery-preview__icon" src={`/static/${gallery}/icon.svg`} alt={`${galleryData.name} icon`}/>
      <span className="gallery-preview__gallery-title">{galleryData.name}</span>
      <span className="gallery-preview__gallery-title-separator">|</span>
      <span className="gallery-preview__gallery-title">{`${galleryData.day}-${galleryData.month}-${galleryData.year}`}</span>
    </div>
    {preview && (<div className="gallery-preview__show-all-container">
      <Link className="gallery-preview__show-all-link" to={`/${gallery}`} >
        <span>Alle {totalImageCount} tonen</span>
        <img src={`${process.env.PUBLIC_URL}/right-arrow.svg`} alt={`See all ${galleryData.name} images`}/>
      </Link>
    </div>)}
  </div>
);

export default GalleryTopBar;