import axios from 'axios';

export const getGalleries = async () => {
  const { status, data: { galleries } } = await axios.get('/galleries');
  if (status !== 200) {
    throw new Error('Could not retrieve galleries');
  }

  return galleries;
};

export const getPreviewImages = async (gallery) => {
  const { status, data: { images } } = await axios.get(`/${gallery}/images?page=1&limit=5`);
  if (status !== 200) {
    throw new Error('Could not retrieve preview images');
  }

  return images;
}

export const getGalleryData = async (gallery) => {
  const { status, data } = await axios.get(`/static/${gallery}/data.json`);
  if (status !== 200) {
    throw new Error('Could not retrieve gallery data');
  }

  return data;
}

export const getGalleryImageCount = async (gallery) => {
  const { status, data: { imageCount } } = await axios.get(`/${gallery}/image_count`);
  if (status !== 200) {
    throw new Error('Could not retrieve image count');
  }

  return imageCount;
}

export const getImages = async (gallery, page = 1, limit = 100) => {
  const { status, data: { images } } = await axios.get(`/${gallery}/images?page=${page}&limit=${limit}`);
  if (status !== 200) {
    throw new Error('Could not retrieve images');
  }

  return images;
}